class Layer {
    constructor(name) {
        this.name = name;
        this.enabled = true;
    }

    start(focus) {
        // const e = this.icon = $('<div>');
        // e.attr('class', 'cell').append(this.layerIcon(focus));
        // $('#menu').append(e);
        this.focus = focus;
        this.enabled = this.enabled; //force update
    }

    stop(focus) {
        this.icon.remove();
        this.icon.html('');
    }

    set enabled(e) {
        //if (this._enabled !== e) {
            this._enabled = e;
            const ele = this.icon;
            if (ele) {
                if (e) ele.addClass   ('cell_enabled');
                else   ele.removeClass('cell_enabled');
            }
        //}
    }

    /** called when position is updated */
    position(pos) {

    }

    get enabled() {
        return this._enabled;
    }

    enable() {
        this.enabled = true;
        return this;
    }

    disable() {
        this.enabled = false;
        return this;
    }

    // layerIcon(focus) {
    //     return $('<button>').text(this.name);
    // }
}
