"use strict";

class Focus {
    constructor(attnElement) {
        this.layers = [];
        this.view = null;
        this.event = mitt();
        this.attn = new graphology.Graph({multi: true, allowSelfLoops: false, type: 'directed'});
        this.active = new Set();
        this.GOAL_EPSILON = 0.05;
        this.spread = _.throttle(() => this._update(), 100);
        this.setupOntology();

        this.ele = attnElement;

        this.tagBrowser = new TagBrowserComponent(this);

        this.setupUI();
        this.loadPresets();
        this.clear();
    }

    setupUI() {
        this.tagContainer = $('#tags').addClass('tag-container');
        this.tagContainerElements = new Map();


        this.tagInput = $('<input type="text" placeholder="Add new tag...">').addClass('tag-input');
        this.presetSelect = $('<select>').addClass('preset-select');
        const savePresetButton = $('<button>').text('Save Preset');
        const presetContainer = $('<div>').addClass('preset-container').append(this.presetSelect, savePresetButton);

        this.presetSelect.on('change', () => this.applyPreset(this.presetSelect.val()));
        savePresetButton.on('click', () => this.savePreset(prompt('Enter a name for this preset:')));

        //this.tagContainer.sortable({ update: () => this.updateFocus() });

        const menu = this.renderMenu();

        this.ele.append(menu, presetContainer, this.tagBrowser.ele);
    }

    renderMenu() {
        const m = $('<div>');
        // $('a[title=Load]').click(() => {
        //     alert('Load');
        // });
        // $('a[title=Save]').click(() => {
        //     alert('Save');
        // });
        // $('a[title=Share]').click(() => {
        //     alert('Share');
        // });
        // $('a[title=Clear]').click(() => {
        //     f.clear();
        // });
        // $('a[title=Dump]').click(() => {
        //     winbox('dump', $('<div>').text(JSON.stringify(f.getFocus())));
        // });
        // $('a[title=Options]').click(() => {
        //     winbox('options', $('<div>').text('options'));
        // });
        // <div id="meta" style="text-align: center">
        //     <a title="Load"><i className="fa fa-folder-open"></i></a>
        //     <a title="Save"><i className="fa fa-save"></i></a>
        //     <a title="Share"><i className="fa fa-share-alt"></i></a>
        //     <a title="Dump"><i className="fa fa-print"></i></a>
        //     <a title="Options"><i className="fa fa-cog"></i></a>
        // </div>
        // <div id="metabottom" style="text-align: center">
        //     <a title="Add"><i className="fa fa-plus-square"></i></a>
        //     <a title="Clear"><i className="fa fa-eraser"></i></a>
        // </div>
        return m;
    }

    setupOntology() {
        const ontology = {
            'way': ['highway', 'cycleway', 'sidewalk', 'oneway', 'maxspeed', 'direction', 'lanes', 'footway',
                'drive_through', 'bicycle', 'bus', 'bridge', 'railway', 'crossing', 'public_transport',
                'lane_markings', 'passenger_lines', 'tracktype', 'turn', 'parking', 'park_ride'],
            'sidewalk': ['sidewalk:left', 'sidewalk:right'],
            'religion': ['denomination'],
            'land': ['landuse', 'surface', 'boundary'],
            'shop': ['fee', 'atm', 'brand', 'advertising'],
            'eat': ['cafe', 'bar', 'pub', 'fast_food', 'happy_hours', 'outdoor_seating']
        };

        Object.entries(ontology).forEach(([parent, children]) => {
            children.forEach(child => this.link(child, parent));
        });
    }

    loadPresets() {
        try {
            this.presets = JSON.parse(localStorage.getItem('focusPresets')) || {};
        } catch (e) {
            console.error("Error loading presets:", e);
            this.presets = {};
        }
        this.updatePresetSelect();
    }

    updatePresetSelect() {
        this.presetSelect.empty().append($('<option>').text('Select a preset...'));
        Object.keys(this.presets).forEach(presetName =>
            this.presetSelect.append($('<option>').text(presetName).val(presetName))
        );
    }

    savePreset(name) {
        if (name) {
            this.presets[name] = this.getFocus();
            localStorage.setItem('focusPresets', JSON.stringify(this.presets));
            this.updatePresetSelect();
        }
    }

    applyPreset(presetName) {
        if (this.presets[presetName]) {
            this.setFocus(this.presets[presetName]);
        }
    }

    setFocus(f) {
        this.tagContainer.empty();
        f.forEach(ff => this.tagEnable(ff.id, true));
        this.event.emit('attn_change');
    }

    getFocus() {
        return Array.from(this.tagContainer.children()).map(child => ({
            id: $(child).data('tag'),
            state: $(child).data('state') || {}
        }));
    }

    getFocusTags() {
        return Array.from(this.active);
        // const f = this.getFocus();
        // const t = new Set();
        // f.forEach(({id: k}) => {
        //     t.add(k);
        //     graphologyLibrary.traversal.dfsFromNode(this.attn, k, (_v) => t.add(_v));
        // });
        // return Array.from(t);
    }

    clear() {
        this.setFocus([]);
    }

    tagEnable(tagName, en) {

        if (!en) {
            const t = this.tagContainerElements.get(tagName);
            if (!t)
                return; //not enabled
            this.tagContainerElements.delete(tagName);
            t.remove();
            this.active.delete(tagName);
        } else {
            if (this.active.has(tagName))
                return; //already enabled

            this.active.add(tagName);

            const tagElement = $('<div>').addClass('tag-chip').text(tagName).data('tag', tagName);

            const closeButton = $('<span>').addClass('remove-tag').text('×').click(() => {
                this.tagEnable(tagName, false);
            });
            tagElement.append(closeButton);

            this.tagContainer.append(tagElement);
            this.tagContainerElements.set(tagName, tagElement);
            this.tagInput.val('');
        }
        this.event.emit('attn_change');
    }

    addNode(x) {
        if (!this.attn.hasNode(x)) {
            this.attn.addNode(x, { });
            return true;
        }
        return false;
    }

    getRootNodes() {
        return this.attn.nodes().filter(node => this.attn.inDegree(node) === 0);
    }

    link(x, y) {
        [x, y].forEach(node => this.addNode(node));
        this.attn.addEdge(y, x, {});
        this.event.emit('graph_change', {});
    }

    _update() {
        const inRate = 0, outRate = 0.75, selfRate = 0, iters = 2;

        for (let iter = 0; iter < iters; iter++) {
            this.attn.forEachNode((_n, n) => {
                if (n.specified) return;

                let v = this.goal(n) * selfRate;
                let sum = selfRate;

                if (outRate !== 0) {
                    this.attn.forEachOutNeighbor(_n, (_x, x) => {
                        v += this.goal(x) * outRate;
                        sum += outRate;
                    });
                }

                this.goalSet(n, sum === 0 ? 0 : v / sum);
            });
        }

        const values = new Map();
        this.attn.forEachNode((_n, x) => {
            const gx = this.goal(x);
            graphologyLibrary.traversal.dfsFromNode(this.attn, _n, (_v, v) => {
                if (v.instance?.renderables) {
                    values.set(_v, (values.get(_v) || 0) + gx);
                }
            });
        });

        this.attn.forEachNode((n, x) => {
            const xi = x.instance;
            if (!xi || !xi.renderables) return;

            const d = values.get(n) || 0;
            const da = Math.abs(d);
            const _red = Math.max(-d, 0);
            const _green = Math.max(+d, 0);

            const enabled = da >= this.GOAL_EPSILON;
            const color = {red: _red, green: _green, blue: 0, alpha: da};
            xi.renderables.forEach(r => {
                r.enabled = enabled;
                if (r.enabled && r.attributes && r.attributes.interiorColor) {
                    Object.assign(r.attributes.interiorColor, color);
                }
            });
        });

        this.view.redraw();
    }

    goal(x) {
        const node = typeof x === 'string' ? this.attn.getNodeAttributes(x) : x;
        return node ? (node.goal || 0) : 0;
    }

    goalSet(x, value) {
        x.goal = value;
    }

    goalAdd(x, dg, update = false) {
        if (Math.abs(dg) < this.GOAL_EPSILON) return;

        let g = x.goal || 0;
        g = Math.max(-1, Math.min(1, g + dg));
        x.goal = g;
        if (update) this.spread();
    }

    position(lat, lon, alt) {
        const pos = this.view.pos();
        if (lat && lon && alt) {
            Object.assign(pos, {latitude: lat, longitude: lon, altitude: alt});
            this.layers.forEach(l => l.position(pos));
        }
        return pos;
    }

    addLayer(layer) {
        this.layers.push(layer);
        if (layer.enabled === undefined) layer.enable();
        layer.position(this.position());
        layer.start(this);
    }

    removeLayer(layer) {
        layer.element.remove();
        delete layer.element;
        this.layers = this.layers.filter(x => x !== layer);
        layer.stop(this);
    }
}

class TagBrowserComponent {
    constructor(focus) {
        this.focus = focus;
        this.ele = document.createElement('div');
        this.ele.className = 'tag-browser';
        this.filterInput = null;
        this.treeRoot = null;
        const renderTreeMenuDebounced = _.throttle(()=>this.renderTreeMenu(), 500);
        this.focus.event.on('graph_change', ()=>{
            renderTreeMenuDebounced();
        });
        this.render();
    }

    render() {
        this.ele.innerHTML = '';
        this.renderTreeMenu();
        this.renderFilterInput();
        return this.ele;
    }

    renderFilterInput() {
        this.filterInput = document.createElement('input');
        this.filterInput.type = 'text';
        this.filterInput.placeholder = 'Filter...';
        this.filterInput.className = 'tag-filter-input';
        this.filterInput.addEventListener('input', () => this.filterTags(this.filterInput.value));

        const filterContainer = document.createElement('div');
        filterContainer.className = 'filter-container';
        filterContainer.appendChild(this.filterInput);
        this.ele.appendChild(filterContainer);
    }

    renderTreeMenu() {
        if (this.treeRoot) this.ele.removeChild(this.treeRoot);

        this.treeRoot = document.createElement('ul');
        this.treeRoot.className = 'tag-tree';
        this.tagHierarchy = this.buildTagHierarchy();
        this.renderTreeNode(this.tagHierarchy, this.treeRoot);
        this.ele.appendChild(this.treeRoot);
    }

    renderTreeNode(node, parentElement) {
        for (const [key, value] of Object.entries(node)) {
            const li = document.createElement('li');
            const span = document.createElement('span');
            span.textContent = key;
            span.className = 'tag-tree-item';
            li.appendChild(span);

            if (Object.keys(value).length > 0) {
                span.classList.add('has-children');
                span.addEventListener('click', () => this.toggleNode(li));
                const childUl = document.createElement('ul');
                childUl.className = 'tag-subtree hidden';
                this.renderTreeNode(value, childUl);
                li.appendChild(childUl);
            } else {
                span.addEventListener('click', () => this.selectTag(key));
            }

            parentElement.appendChild(li);
        }
    }

    buildTagHierarchy() {
        const h = {};
        this.focus.getRootNodes().forEach(node => {

            //TODO make fully recursive
            const childNodes = this.focus.attn.outNeighbors(node);

            if (childNodes.length > 0) {
                const z = {};

                _.forEach(childNodes, (v) => {
                    const cc = this.focus.attn.outNeighbors(v);
                    if (cc.length > 0) {
                        z[v] = { };
                    }

                });

                h[node] = z;
            }
        });
        return h;



        // This is a placeholder. In a real implementation, you would build
        // the hierarchy based on your tag data structure.
        // return {
        //     'Geography': {
        //         'Countries': {
        //             'USA': {},
        //             'Canada': {},
        //             'Mexico': {}
        //         },
        //         'Cities': {},
        //         'Landmarks': {}
        //     },
        //     'Nature': {
        //         'Animals': {},
        //         'Plants': {},
        //         'Weather': {}
        //     },
        //     'Technology': {
        //         'Software': {},
        //         'Hardware': {},
        //         'Internet': {}
        //     }
        // };
    }

    toggleNode(li) {
        const childUl = li.querySelector('ul');
        if (childUl) {
            childUl.classList.toggle('hidden');
            li.querySelector('.tag-tree-item').classList.toggle('expanded');
        }
    }

    selectTag(tag) {
        this.focus.tagEnable(tag, true);
    }

    filterTags(query) {
        query = query.toLowerCase();
        const allItems = this.ele.querySelectorAll('.tag-tree-item');

        allItems.forEach(item => {
            const shouldShow = item.textContent.toLowerCase().includes(query);
            item.style.display = shouldShow ? '' : 'none';

            if (shouldShow) {
                let parent = item.parentElement;
                while (parent && !parent.classList.contains('tag-tree')) {
                    if (parent.tagName === 'UL') {
                        parent.classList.remove('hidden');
                    }
                    if (parent.tagName === 'LI') {
                        parent.style.display = '';
                    }
                    parent = parent.parentElement;
                }
            }
        });
    }
}