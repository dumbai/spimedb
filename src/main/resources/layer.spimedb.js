class SpimeDBLayer extends GeoLayer {
    constructor(name, url) {
        super(name, new WorldWind.RenderableLayer(name));
        this.url = url || ("ws://" + window.location.host);
        this.socket = undefined;
        this.cache = new Map(); //TODO LRU
        this.active = new Map();
    }

    start(f) {
        this.reconnect(f);

        super.start(f);
    }

    pointVisible(latitude, longitude, altitude, f) {
        const p = WorldWind.Vec3.zero();
        f.view.w.globe.computePointFromPosition(latitude, longitude, altitude, p);
        return f.view.w.drawContext.frustumInModelCoordinates.containsPoint(p);
    }


    _update(f) {

        if (this.updateInProgress) {
            this.cancelPendingUpdates();
        }
        this.updateInProgress = true;

        const { latitude: lat, longitude: lon } = f.view.pos();

        const fov = f.view.w.camera.fieldOfView;
        const viewport = f.view.w.viewport;

        const frustum = f.view.w.drawContext.frustumInModelCoordinates;

        const nearDistance = frustum.far.distance, farDistance = frustum.near.distance;

        const aspectRatio = viewport.width / viewport.height;

        const visibleRegion = this.calculateVisibleRegion(lat, lon, fov, aspectRatio, nearDistance, farDistance);
        //const subregions = this.divideIntoSubregions(visibleRegion, lat, lon);
        //subregions.sort((a, b) => a.distance - b.distance);

        const subregions = [ visibleRegion ];

        const tags = f.getFocusTags();
        this.pendingUpdates = subregions.map(subregion => {
            const lod = this.calculateLOD(subregion.distance);
            return this.getAll(subregion, "id", tags);
        });
    }


    calculateVisibleRegion(lat, lon, fov, aspectRatio, nearDistance, farDistance) {
        const RADIANS_PER_DEGREE = (2*Math.PI/360);
        const DEGREES_PER_RADIAN = 360/(2*Math.PI);

        fov *= RADIANS_PER_DEGREE; //to radians

        const nearHeight = 2 * Math.tan(fov / 2) * nearDistance;
        const nearWidth = nearHeight * aspectRatio;

        const farHeight = 2 * Math.tan(fov / 2) * farDistance;
        const farWidth = farHeight * aspectRatio;

        const latDelta = (farHeight / 2) / WorldWind.EARTH_RADIUS;
        const lonDelta = (farWidth / 2) / (WorldWind.EARTH_RADIUS * Math.cos(lat * RADIANS_PER_DEGREE));


        return {
            latMin: lat - latDelta * DEGREES_PER_RADIAN,
            latMax: lat + latDelta * DEGREES_PER_RADIAN,
            lonMin: lon - lonDelta * DEGREES_PER_RADIAN,
            lonMax: lon + lonDelta * DEGREES_PER_RADIAN
        };
    }

    divideIntoSubregions(visibleRegion, centerLat, centerLon) {
        const { latMin, latMax, lonMin, lonMax } = visibleRegion;
        const latStep = (latMax - latMin) / 3;
        const lonStep = (lonMax - lonMin) / 3;

        return Array.from({ length: 3 }, (_, i) => Array.from({ length: 3 }, (_, j) => {
            const subLatMin = latMin + i * latStep;
            const subLatMax = subLatMin + latStep;
            const subLonMin = lonMin + j * lonStep;
            const subLonMax = subLonMin + lonStep;

            const subCenterLat = (subLatMin + subLatMax) / 2;
            const subCenterLon = (subLonMin + subLonMax) / 2;

            const distance = this.calculateDistance(centerLat, centerLon, subCenterLat, subCenterLon);

            return {
                latMin: subLatMin,
                latMax: subLatMax,
                lonMin: subLonMin,
                lonMax: subLonMax,
                distance
            };
        })).flat();
    }

    calculateDistance(lat1, lon1, lat2, lon2) {
        const [φ1, λ1, φ2, λ2] = [lat1, lon1, lat2, lon2].map(angle => angle * WorldWind.RADIANS_PER_DEGREE);
        const Δφ = φ2 - φ1;
        const Δλ = λ2 - λ1;

        const a = Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
                  Math.cos(φ1) * Math.cos(φ2) *
                  Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return WorldWind.EARTH_RADIUS * c;
    }

    calculateLOD(distance) {
        if (distance < 1000) {
            return 0;
        } else if (distance < 10000) {
            return 1;
        } else {
            return 2;
        }
    }

    async getAll({ latMin, latMax, lonMin, lonMax }, output, tagsInclude/*, lod*/) {
        const query = {
            '_': 'earth',
            '@': [lonMin, lonMax, latMin, latMax],
            output
            /*lod*/
        };
        if (tagsInclude.length > 0) {
            query.in = tagsInclude;
        }
        return this.socket.send(JSON.stringify(query));
    }

    cancelPendingUpdates() {
//        if (this.pendingUpdates) {
//            this.pendingUpdates.forEach(update => update.cancel());
//            this.pendingUpdates = null;
//        }
    }

    pickPos(x, y, points, f) {
        const xy = f.view.w.pickTerrain(new WorldWind.Vec2(x, y));
        if (xy.objects.length > 0) {
            points.push(xy.objects[0].position);
        }
    }


    reconnect(f) {
        if (this.socket) {
            this.close();
        }

        this.socket = new WebSocket(this.url);
        this.socket.binaryType = 'arraybuffer';

        this.socket.onopen = () => {
            if (!this.view_change) {
                let changer = _.debounce(() => {
                    this._update(f);
                }, 50);
                this.view_change = [ f.event.on('view_change', changer), f.event.on('attn_change', changer) ];
            }

            this.socket.send("{'_':'tag'}");
            this._update(f);
        };
        // socket.addEventListener('close', closeConnection);
        this.socket.onmessage = async (x) => {
            let d;
            if (x.data instanceof ArrayBuffer) {
                d = await cbor.decode(new Uint8Array(x.data));
            } else {
                d = JSON.parse(x.data);
            }
            this.onMessage(d, f);
        };
        this.socket.onclose = () => {
            this.close();
        };
        this.socket.onerror = (e) => {
            console.error(e);
        };
    }

    onMessage(d, f) {
        if (d.full)
            this.addAll(d.full, f);
        else if (d.id)
            this.addAllID(d.id, f);
        else if (d.tag)
            this.tag(d.tag, f);
        else
            console.warn('unknown message', d);
    }

    tag(tags, f) {
        //console.log(f);
        // const w = $('<div>').css({
        //     'position': 'relative',
        //     'max-width': '100%',
        //     'max-height': '100%',
        //     'overflow': 'auto',
        //     'background-color': 'black',
        //     'color': 'orange'
        // });
        // for (const t of tags)
        //     w.append($('<div>').text(JSON.stringify(t)));

        for (const t of tags)
            f.addNode(t);

        //const ww = this.newTableWindow(tags);

        // new DataTable(ww, {
        //     data: data,
        //     paging: false,
        //     columns: [
        //         {
        //             title: 'Enable',
        //             // render: (data, type) =>
        //             //     '<button>' + data + '</button>'
        //         },
        //         { title: 'Tag' },
        //         { title: 'Count' }
        //     ]
        // });

        //winbox("Tags", w.append(ww));
    }

    newTableWindow(tags) {
        const data = _.map(tags, t => {
            const v = {};
            v.enable = true;
            v.label = t.label;
            v.count = t.value;
            return v;
        });

        const ww = $('<table>'); //.css('width', '100%');

        /* https://datatables.net/manual/options */
        const table = new Tabulator(ww[0], {
            width: '100%',
            resizableColumnFit: true,
            layout: "fitColumns",
            data: data,
            columns: [
                {
                    title: "Enable", field: "enable", sorter: "boolean",
                    formatter: 'tickCross', editor: "tickCross"
                },
                {title: "Name", field: "label", sorter: "string"},
                {title: "Count", field: "count", sorter: "number"},
                // {title:"Rating", field:"rating", formatter:"star", hozAlign:"center", width:100},
                // {title:"Favourite Color", field:"col", sorter:"string"},
                // {title:"Date Of Birth", field:"dob", sorter:"date", hozAlign:"center"},
                // {title:"Driver", field:"car", hozAlign:"center", formatter:"tickCross", sorter:"boolean"},
            ],
        });
        table.on("cellEdited", function (cell) {
            //console.log('edited', cell);
            const row = cell._cell.row.data;
            const colField = cell._cell.column.field;

        });
        return ww;
    }

    addAllID(xx, f) {
        this.active.forEach((v, k) => {
            v.visible = false;
        });

        const need = [];

        for (const x of xx) {
            const X = this.cache.get(x);
            if (X) {
                X.visible = true;
                this.active.set(x, X);
            } else {
                need.push(x);
            }
        }
        if (need.length > 0) {
            this.socket.send(
                JSON.stringify({"_":"getAll",id:need}, null)
                //'{"_":"getAll", "id":' + JSON.stringify(need, null) + '}'
            );
        } else {
            this.addAll([], f); //just commit
        }
        //console.log('refresh', needFull.length + '/' + xx.length);
    }

    addAll(d, f) {

        const renderables = [];

        for (const i of d) {
            //INSTANTIATE:

            const tags = i['>'];
            const id = i.I;

            if (tags) {
                if (_.isArray(tags)) {
                    for (const t of tags)
                        f.link(id, t);
                } else {
                    f.link(id, tags);
                }
            }

            // _.forEach(i, (value, key) => {
            //     if (key === 'id' || key === 'name' || key === 'ele' || key.startsWith('addr') || key==='odbl' || key==='layer' || key === 'website' || key.startsWith('source') || key.startsWith('gnis') || key.startsWith('tiger') || key.startsWith('brand:'))
            //         return;
            //
            //     //custom rewrites:
            //     if (key === 'foot') key = 'walkable';
            //
            //     const keyvalue = key + '=' + value;
            //     f.link(i, keyvalue);
            //     f.link(keyvalue, key);
            //     if (value!=='yes' && value!=='no' && !_.isNumber(parseFloat(value)))
            //         f.link(keyvalue, value);
            //     //const xx = f.attn.getNodeAttributes(i);
            //     //xx.instance = x;
            //     //xx.style('display', 'none');
            // });

            //console.log(i);
            // const prev = this.cache.get(i.I);
            // if (prev) {
            //     console.log('rev remvomv');
            //     this.layer.removeRenderable(prev.renderable); //HACK
            // }
            this.cache.set(id, i);
            this.active.set(id, i);
            // const cfg = {};
            // cfg.attributes = new WorldWind.ShapeAttributes(null);
            // cfg.attributes.drawOutline = true;
            // cfg.attributes.outlineColor = new WorldWind.Color(
            //     0.1 * cfg.attributes.interiorColor.red,
            //     0.3 * cfg.attributes.interiorColor.green,
            //     0.7 * cfg.attributes.interiorColor.blue,
            //     1.0);
            // cfg.attributes.outlineWidth = 2.0;

            if (i["g*"]) {
                //POLYGON
                // const pathPositions = _.map(i["g*"], p => new WorldWind.Location(p[0], p[1], 1 /* TODO */));
                // const p = new WorldWind.SurfacePolygon(pathPositions, null);
                const pathPositions = _.map(i["g*"], p => new WorldWind.Position(p[0], p[1], 1 /* TODO */));
                const p = new WorldWind.Polygon(pathPositions, null);
                p.altitudeMode = WorldWind.RELATIVE_TO_GROUND;
                //p.altitudeMode = WorldWind.CLAMP_TO_GROUND;
                //path.attributes.extrude = true;
                p.attributes.drawInterior = true;
                p.attributes.drawOutline = false;
                p.attributes.interiorColor = new WorldWind.Color(1, 0.5, 0, 0.9);
                p.nobject = i; renderables.push(i.renderable = p);
            } else if (i["g-"]) {
                //LINESTRING (path)

                const pathPositions = _.map(i["g-"], p => new WorldWind.Position(p[0], p[1], 1 /* TODO */));
                const p = new WorldWind.Path(pathPositions, null);

                //p.altitudeMode = WorldWind.RELATIVE_TO_GROUND; // The path's altitude stays relative to the terrain's altitude.
                p.altitudeMode = WorldWind.CLAMP_TO_GROUND;

                p.followTerrain = true;
                p.extrude = false; // Make it a curtain.
                p.useSurfaceShapeFor2D = false; // Use a surface shape in 2D mode.

                // const pathPositions = _.map(i["g-"], p => new WorldWind.Location(p[0], p[1], 0 /* TODO */));
                // const p = new WorldWind.SurfacePolyline(pathPositions, null);
                // //path.attributes.extrude = true;
                // p.attributes.drawInterior = false;
                // p.attributes.drawOutline = true;
                p.attributes.outlineWidth = 5;
                p.attributes.outlineColor = new WorldWind.Color(1, 0, 1, 0.75);


                p.nobject = i; renderables.push(i.renderable = p);


            } else if (i["@"]) {
                //point
                const ii = i["@"];
                const pos = new WorldWind.Position(ii[2], ii[1], ii[3]);

                const p = new WorldWind.Placemark(pos, true);
                if (i.N)
                    p.label = i.N;
                // else if (i['>'])
                //     point.label = i[">"];

                p.altitudeMode = WorldWind.RELATIVE_TO_GROUND;


                const placemarkAttributes = new WorldWind.PlacemarkAttributes(null);
                placemarkAttributes.imageScale = 0.5;
                placemarkAttributes.imageColor = new WorldWind.Color(1, 0, 0, 0.5);
                // placemarkAttributes.labelAttributes.offset = new WorldWind.Offset(
                //     WorldWind.OFFSET_FRACTION, 0.5,
                //     WorldWind.OFFSET_FRACTION, 1.5);
                placemarkAttributes.imageSource = WorldWind.configuration.baseUrl + "images/white-dot.png";
                p.attributes = placemarkAttributes;

                p.nobject = i; renderables.push(i.renderable = p);
            } else {
                console.error("unhandled geometry type: ", i);
            }
            i.visible = true;
            this.active.set(id, i);
        }
        this.active.forEach((v,k) => {
            if (v.visible) {
                if (v.hidden) {
                    //reactivate
                    delete v.hidden;
                    renderables.push(v.renderable);
                }
            } else {
                if (this.active.delete(k)) {
                    if (v.renderable) {
                        this.layer.removeRenderable(v.renderable);
                        v.hidden = true;
                    }
                }
            }
        });

        for (const r of renderables) {
            this.layer.addRenderable(r);
        }

        this.updateInProgress = false;

        this.focus.event.emit('graph_change');
        //console.log(this.active.size, 'active', this.cache.size, 'cached');
        f.view.redraw();
    }


    close() {
        this.socket.close();
        delete this.socket;
    }

// changeConnection(event) {
    //     // open the connection if it's closed, or close it if open:
    //     if (socket.readyState === WebSocket.CLOSED) {
    //         openSocket(serverURL);
    //     } else {
    //         socket.close();
    //     }
    // }


    // closeConnection() {
    //     this.socket.close();
    // }


    // function sendMessage() {
    //     //if the socket's open, send a message:
    //     if (socket.readyState === WebSocket.OPEN) {
    //         socket.send(outgoingText.value);
    //     }
    // }

    stop(focus) {
        super.stop(focus);
        this.layer.removeAllRenderables();
    }

}
